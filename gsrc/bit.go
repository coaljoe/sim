package main

type BitType int

const (
	BitType_Empty BitType = iota
	BitType_Oil
	BitType_Wood
)

type Bit struct {
	btype       BitType
	material_id int
	flameable   *Flameable
	temp        float64
	dirty       bool
}

func new_bit() *Bit {
	return &Bit{
		temp:  20,
		dirty: false,
	}
}

func (b *Bit) set_material(m *Material) {
	b.material_id = m.id
}

func (b *Bit) get_material() *Material {
	return material_sys.get_material_by_id(b.material_id)
}

func (b *Bit) update(dt float64) {
	if b.flameable != nil {
		b.flameable.update(dt)
	}
}

//func (b *Bit) String() string {
//	return "<Bit>"
//}

// Flameable
//
type Flameable struct {
	bit          *Bit
	flameTime    float64
	curFlameTime float64
	active       bool
}

func new_flameable(b *Bit) Flameable {
	return Flameable{bit: b}
}

func (f *Flameable) ignite() {
	p("Flameable: igniting flameable;  temp:", f.bit.temp)
	f.active = true
}

func (f *Flameable) update(dt float64) {
	p("Flameable: flameable update")
}
