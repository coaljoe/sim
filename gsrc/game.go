package main

var (
	material_sys *MaterialSys
	world_bs     *Bitspace
	burning_sys  *BurningSys
	temp_sys     *TempSys
)

func init_game() {
	material_sys = new_material_sys()
	//world_bs = new_bitspace(2, 2, 1)
	//world_bs = new_bitspace(3, 3, 3)
	//world_bs = new_bitspace(2*10, 2*10, 0.5*10)
	//world_bs = new_bitspace(5*10, 5*10, 0.5*10)
	//world_bs = new_bitspace(10*10, 10*10, 0.1*10)
	world_bs = new_bitspace(100*10, 100*10, 0.1*10)
	temp_sys = new_temp_sys()
	burning_sys = new_burning_sys()
}

func update_game(dt float64) {
	temp_sys.update(dt)
	//burning_sys.update(dt)
}
