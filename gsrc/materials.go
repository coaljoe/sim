package main

func make_empty_material() *Material {
	m := new_material()
	material_sys.addElem(m)
	return m
}

func make_wood_material() *Material {
	m := make_empty_material()
	m.name = "Wood"
	m.typename = "wood"
	m.termalConductivity = 0.12
	m.igniteTemp = 250
	m.density = 900
	return m
}

func make_oil_material() *Material {
	m := make_empty_material()
	m.name = "Oil"
	m.typename = "oil"
	m.termalConductivity = 0.15
	m.igniteTemp = 80
	m.density = 800
	return m
}
