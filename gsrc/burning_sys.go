package main

type BurningSys struct {
}

func new_burning_sys() *BurningSys {
	return &BurningSys{}
}

func (s *BurningSys) update(dt float64) {
	bs := world_bs
	for _, b := range bs.all() {
		//println("->", bs, i, bs.bits[i])
		if b.flameable != nil {
			// если температура больше т. возгорания - зажечь бит
			if b.temp >= b.get_material().igniteTemp {
				b.flameable.ignite()
			}
		}
	}
}
