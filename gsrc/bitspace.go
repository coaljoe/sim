package main

type Bits []*Bit

type Bitspace struct {
	bits    Bits
	w, h, d int
}

func new_bitspace(w, h, d int) *Bitspace {
	bits := make(Bits, w*h*d)
	bs := &Bitspace{
		bits: bits,
		w:    w,
		h:    h,
		d:    d,
	}
	return bs
}

func (bs *Bitspace) size() int {
	return bs.w * bs.h * bs.d
}

func (bs *Bitspace) fill(b *Bit) {
	for i := range bs.bits {
		bs.bits[i] = b
	}
}

func (bs *Bitspace) idx_to_xyz(i int) (int, int, int) {
	//idx := (bs.h*bs.w*z + bs.w*y) + x
	k := i
	x := k / (bs.w * bs.d)
	w_ := k % (bs.w * bs.d)
	y := w_ / bs.d
	z := w_ % bs.d
	return x, y, z
}

func (bs *Bitspace) all() Bits {
	return bs.bits
}

func (bs *Bitspace) has(x, y, z int) bool {
	idx := (bs.h*bs.w*z + bs.w*y) + x
	//println("derp:", bs.size(), idx)
	return (idx >= 0) && (idx < bs.size())
}

func (bs *Bitspace) at(x, y, z int) *Bit {
	idx := bs.h*bs.w*z + bs.w*y
	return bs.bits[idx+x]
}

func (bs *Bitspace) set(x, y, z int, b *Bit) {
	idx := bs.h*bs.w*z + bs.w*y
	bs.bits[idx+x] = b
}

/*
func (bs *Bitspace) get_schunk2(x, y, z, w, h int) [][]*Bit {
	r := make([][]*Bit, s)
	for i := range r {
		r[i] = make([]*Bit, s)
	}
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			if bs.has(x, y, z) {
				r[x][y] = bs.at(x, y, z)
			}
		}
	}
	return r
}
*/
func (bs *Bitspace) get_schunk(x, y, z, w, h int) Bits {
	r := make([]*Bit, w*h)
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			if bs.has(x, y, z) {
				//println("->", (y*w)+x, x, y, z)
				r[(y*w)+x] = bs.at(x, y, z)
				//r[0] = bs.at(0, 0, 0)
			}
		}
	}
	return r
}

func (bs *Bitspace) get_neighbours(x, y, z int) []*Bit {
	var r []*Bit
	//r[0] = bs.at(
	s1 := bs.get_schunk(x, y, z+1, 3, 3)
	s2 := bs.get_schunk(x, y, z, 3, 3)
	s3 := bs.get_schunk(x, y, z-1, 3, 3)

	s2 = append(s2[:4], s2[5:]...)

	r = append(r, s1...)
	r = append(r, s2...)
	r = append(r, s3...)

	return r
}
