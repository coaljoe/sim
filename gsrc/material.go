package main

import "github.com/fmstephe/simpleid"

var next_id = simpleid.NewIdMaker()

type Material struct {
	id                  int
	name                string
	typename            string
	density             float64 // kg/m3
	hardness            float64
	igniteTemp          float64
	corrosionResistance float64
	termalConductivity  float64 // W/m K
}

func new_material() *Material {
	return &Material{
		id:                  int(next_id.Id() - 1),
		name:                "default",
		density:             1000,
		corrosionResistance: 0,
		igniteTemp:          100.0,
		termalConductivity:  1,
	}
}

func (m *Material) set_density(v float64) {
	m.density = v
}

func (m *Material) get_density(v float64) float64 {
	return m.density
}

func (m *Material) String() string {
	return sp("<Material id:", m.id, "name:", m.name, ">")
}
