package main

import (
	"fmt"
	"reflect"
	//"gopkg.in/fatih/set.v0"
	//"github.com/emirpasic/gods/sets/hashset"
	"github.com/emirpasic/gods/lists/arraylist"
	//"github.com/emirpasic/gods/sets/treeset"
	//"github.com/emirpasic/gods/maps/treebidimap"
	//"container/list"
)

type hasName interface {
	Name() string
}

type System struct {
	name  string
	desc  string
	elems *arraylist.List
}

func newSystem(name, desc string) *System {
	/*
		idComparator := func(a, b interface{}) {
			a.(hasId).Id() - b.(hasId).Id()
		}
	*/
	s := &System{
		name:  name,
		desc:  desc,
		elems: arraylist.New(),
	}
	return s
}

func (s *System) hasElem(el interface{}) bool {
	return s.elems.Contains(el)
}

func (s *System) idxElem(el interface{}) int {
	foundIndex, _ := s.elems.Find(
		func(index int, value interface{}) bool {
			return el == value
		})
	return foundIndex // -1 if not found
}

func (s *System) addElem(el interface{}) {
	if s.hasElem(el) {
		_log.Err("already have element", el, &el)
		panic("addElem failed")
	}
	s.elems.Add(el)
	_log.Inf("added element '" + reflect.TypeOf(el).String() + "' to system '" + s.name + "'")
}

func (s *System) removeElem(el interface{}) {
	p("have it:", s.hasElem(el))
	idx := s.idxElem(el)
	if idx == -1 {
		_log.Err("element not found", el, &el)
		panic("removeElem failed")
	}
	s.elems.Remove(idx)
	_log.Inf("removed element '" + reflect.TypeOf(el).String() + "' from system '" + s.name + "'")
}

func (s *System) getElems() []interface{} {
	return s.elems.Values()
}

func (s *System) listElems() {
	fmt.Println("list elements:")
	for n, el := range s.getElems() {
		name := "unknown"
		if xel, ok := el.(hasName); ok {
			name = xel.Name()
		}
		fmt.Printf(" -> %d: %s\n", n, name)
	}
	fmt.Println("end")
}
