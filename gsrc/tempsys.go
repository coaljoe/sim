package main

import (
//"math"
)

type TempSys struct {
}

func new_temp_sys() *TempSys {
	return &TempSys{}
}

func (s *TempSys) update(dt float64) {
	bs := world_bs
	// temperature transfer
	for i, cb := range bs.all() {
		x, y, z := bs.idx_to_xyz(i)
		nb := bs.get_neighbours(x, y, z)
		for _, b := range nb {
			//if b == nil {
			//		continue
			//}

			// calculate heat transfer

			// q = k A dT / s
			dT := fmax(b.temp, cb.temp) - fmin(b.temp, cb.temp)
			//dT := math.Abs(float64(b.temp - cb.temp))
			if dT < 1.0 {
				continue
			}
			A := 0.1 // 0.1 m2
			k := cb.get_material().termalConductivity
			s := 0.1 // 0.1 m
			q := (k * A * A * dT) / s
			//p("q =", q)

			tempInc := q // / 1000.0
			//p("b.temp before :", b.temp)
			b.temp += tempInc
			//b.temp += float32(tempInc)
			//p("b.temp after  :", b.temp)

			//b.temp = tdiff * cb.get_material().density
		}
	}
}
