package main

import (
	"fmt"
)

func fmax(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}

func fmin(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

func p(a ...interface{}) {
	fmt.Println(a...)
}

func sp(a ...interface{}) string {
	return fmt.Sprintln(a...)
}

func temp_ctok(tk float64) float64 {
	return tk + 273.15
}

func temp_ktoc(tk float64) float64 {
	return tk - 273.15
}
