package main

type MaterialSys struct {
	*System
}

func new_material_sys() *MaterialSys {
	return &MaterialSys{
		System: newSystem("materialsys", "Material System"),
	}
}

func (s *MaterialSys) get_material_by_id(id int) *Material {
	var m *Material
	//s.listElems()
	for _, mi := range s.getElems() {
		m = mi.(*Material)
		//p("->", m.id)
		if m.id == id {
			return m
		}
	}
	//return m
	panic(sp("material not found. id=", id))
}
