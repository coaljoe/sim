package main

import (
	"fmt"
	"lib/xlog"
	"math/rand"
	"os"
	"runtime"
	//"time"
)

func init_app() {
	println("Init() begin")

	// seed
	//rand.Seed(time.Now().UTC().UnixNano())

	// create logger
	_log = xlog.NewLogger("ltest", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("sim.log")
	_log.SetWriteTime(true)
	println("Init() done")
}

func main() {
	fmt.Println("main()")
	//rand.Seed(time.Now().UnixNano())
	init_app()

	init_game()

	m1 := make_wood_material()
	m2 := make_oil_material()
	//m2.set_density(10)

	b1 := make_wood_bit()
	b2 := make_oil_bit()

	fmt.Println(m1, m2, b1, b2)

	//bs := new_bitspace(2, 2, 1)
	bs := world_bs
	//fmt.Println(bs.all())
	bs.fill(b1)

	for x := 0; x < bs.w; x++ {
		for y := 0; y < bs.h; y++ {
			for z := 0; z < bs.d; z++ {
				if rand.Float64() > 0.5 {
					bs.set(x, y, z, b2)
					b := bs.at(x, y, z)
					//b.temp = 9999
					b.temp = 300
					println("derp")
					println(x, y, z)
				}
			}
		}
	}

	//fmt.Println(bs.all())

	fmt.Println(bs.get_neighbours(0, 0, 0))

	fmt.Println("b0:", bs.at(0, 0, 0))

	p("game update")
	update_game(0.1)
	for i := 0; i < 10; i++ {
		//update_game(0.1)
	}

	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)
	p("-- mem stats --")
	p("Alloc:", mem.Alloc/1024, "k")
	p("TotalAlloc", mem.TotalAlloc/1024, "k")
	p("HeapAlloc", mem.HeapAlloc/1024, "k")
	p("HeapSys", mem.HeapSys/1024, "k")

	fmt.Println("exit")
}
