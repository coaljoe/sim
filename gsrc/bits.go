package main

// factories

func make_bit() *Bit {
	return new_bit()
}

func make_oil_bit() *Bit {
	b := make_bit()
	b.btype = BitType_Oil

	// add flameable
	f := new_flameable(b)
	f.flameTime = 10
	b.flameable = &f

	return b
}

func make_wood_bit() *Bit {
	b := make_bit()
	b.btype = BitType_Wood

	// add flameable
	f := new_flameable(b)
	f.flameTime = 10
	b.flameable = &f

	return b
}
