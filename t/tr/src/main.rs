use std::rc::Rc;
use std::cell::RefCell;
mod engine;
use engine::*;
//use self::engine::*;

#[derive(Debug,Clone)]
pub struct Vehicle {
  // props:
  pub info_max_speed: i32,
  // state:
  pub speed: f32,
  pub weight: i32, // XXX state?
  //accel: f32,
  pub engine: Option<Engine>,
  //pub parent: Option<Box<Vehicle>>,
  //pub child: Option<Box<Vehicle>>,
  pub parent: Option<Rc<RefCell<Vehicle>>>,
  pub child: Option<Rc<RefCell<Vehicle>>>,
}

impl Vehicle {
  pub fn new() -> Vehicle {
    Vehicle {
      info_max_speed: 100,
      speed: 0.0,
      weight: 100,
      engine: None,
      parent: None,
      child: None,
    } 
  }

  pub fn has_engine(&self) -> bool {
  	match self.engine {
  		None => false,
  		Some(_) => true,
  	}
  }

  pub fn has_child(&self) -> bool {
  	self.child.is_some()
  }

  pub fn has_parent(&self) -> bool {
  	self.parent.is_some()
  }
}

#[derive(Debug,Clone)]
pub struct TrainCar {
	id: i32,
	vehicle: Vehicle,
	locomotive: bool,
}

impl TrainCar {
	pub fn new() -> TrainCar {
		TrainCar {
			id: 0,
			vehicle: Vehicle::new(),
			locomotive: false,
		}
	}
}

#[derive(Debug,Clone)]
pub struct Train {
	pub cars: Vec<TrainCar>
}

impl Train {
	pub fn new() -> Train {
		let t = Train { cars: Vec::new() };
		t
	}

	pub fn add_car(&mut self, c: TrainCar) {
		self.cars.push(c);
	}

	pub fn get_speed(&self) -> Result<f32, &'static str> {
		if self.cars.len() < 1 {
			Err("no cars")
		} else {
			Ok(self.cars[0].vehicle.speed)
		}
	}
}

fn main() {
    println!("Hello, world!");
    let mut t = Train::new();
    let mut tc1 = TrainCar::new();
    let mut tc2 = TrainCar::new();
    //t.add_car(tc1);
    println!("speed: {}", tc1.vehicle.has_child());
    //tc1.vehicle.child = Some(Box::new(tc2.vehicle));
    //tc2.vehicle.parent = Some(Box::new(tc1.vehicle));
    //tc1.vehicle.child = Some(Rc::new(RefCell::new(Box::new(tc2.vehicle))));
    //tc2.vehicle.parent = Some(Rc::new(RefCell::new(Box::new(tc1.vehicle))));    
    
    //let boxed_tc2 = Rc::new(RefCell::new(tc2.vehicle));
    //let boxed_tc1 = Rc::new(RefCell::new(tc1.vehicle));
    //tc1.vehicle.child = Some(boxed_tc2.clone());
    //tc2.vehicle.parent = Some(boxed_tc1.clone());

	let boxed_tc2 = Rc::new(RefCell::new(tc2));
    let boxed_tc1 = Rc::new(RefCell::new(tc1));
    //tc1.vehicle.child = Some(boxed_tc2.clone().as_ref().borrow_mut().vehicle);
    //let ref x = boxed_tc1.clone().as_ref().borrow_mut().vehicle;
    //let ref x = boxed_tc1.clone().borrow_mut().vehicle;
    {
    let xx = boxed_tc1.clone();
    let x = xx.borrow_mut().to_owned();
    println!("= {:?}", x.vehicle);
    
    //let y = x.borrow_mut();
    //tc2.vehicle.parent = Some(Rc::new(RefCell::new(x.vehicle)));
    let q = boxed_tc2.clone();
    let mut q2 = q.borrow_mut().to_owned();
    //q2.vehicle.parent = Some(Rc::new(RefCell::new(x.vehicle)));
    q2.vehicle.speed = 999.;
    println!("= {:?}", q2.vehicle.speed);
    

    let xxx = boxed_tc2.clone();
    let yyy = xxx.borrow().to_owned().vehicle;
     
    println!("= {:?}", yyy);
    
    }
    
               
    //println!("speed: {}", tc1.vehicle.has_child());
    //t.add_car(tc1);
    println!("has child: {}", boxed_tc1.borrow().vehicle.has_child());
    t.add_car(boxed_tc1.borrow().clone());
    t.add_car(boxed_tc2.borrow().clone());
    println!("has parent: {}", boxed_tc2.borrow().vehicle.has_parent());
    println!("train: {:#?}", t);
    println!("speed: {}", t.get_speed().unwrap());
    println!("train: {:#?}", boxed_tc2);
	println!("train: {:#?}", boxed_tc1);
}
