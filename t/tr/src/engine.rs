#[derive(Debug,Copy,Clone)]
pub struct Engine {
  // props:
  // maximum power, kw/h
  pub info_power: i32,
  // state:
  pub enabled: bool,
  // current power
  pub power: i32,
}

impl Engine {
  pub fn new() -> Engine {
    Engine {
      info_power: 100,
      power: 0,
      enabled: false,
    }
  }

  pub fn enable(&mut self) {
  	self.enabled = true
  }

  pub fn disable(&mut self) {
  	self.enabled = false
  }

  pub fn update(&mut self, dt: f32) {
    if !self.enabled {
      return
    }

    self.power = self.info_power * 1
  }
}