#![cfg_attr(feature="nightly", feature(const_fn))]

#[macro_use]
extern crate lazy_static;
extern crate singleton;
extern crate rand;
extern crate ndarray;

mod hello;
mod game;
use game::*;

fn main() {
    println!("Hello, world!");
	hello::print_hello();

	
	let m = Material::new();
	//let m2 = &m;
	println!(1);
	{
		//println!("{}", m2.density());
	}
	println!("m: {:?}", m);
	//println!("m: {:?}", m2);
	//println!("{}", m2.density())
	println!("{}", m.hardness);

    // mat 2
    let mut m2 = Material::new();
    m2.set_density(10);

    // register
    //material_sys.singleton().get_mut().unwrap().add(m);
    //material_sys.singleton().get_mut().unwrap().add(m2);
    material_sys().derp();
    material_sys().add(m);
    material_sys().add(m2);

    let b1 = Bit::new();
    let b2 = Bit::new();

    /*
	
	let mut bd = BitData {
		id: 0,
		material: m,
		temp: 0.0,
	};
	
	let f = Flameable::new(bd);
	
	let b1 = Bit::EmptyBit;
	let b2 = Bit::OilBit (bd, f);
	
	/*
	{
		// destruct
		//let Bit::OilBit(bd1, f1) = b2;
		//f1.ignite();
		match b2 {
			Bit::EmptyBit =>
				println!("it's empty"),
			Bit::OilBit(bd1, f1) => 
				f1.ignite(),
			_ => (),
		}
	}
	*/
	
	let mut bits = vec![b1, b2];

	for b in bits {
		match b {
			Bit::EmptyBit =>
				println!("empty bit"),
			Bit::OilBit(bd, mut f1) =>
				{
					f1.ignite();
					f1.update(1.0);
					for i in 0 .. 10 {
						f1.update(1.0);
					}
				}
			_ => (),
		}
	}
    */
	
	println!("calling Bitspace::new()");
	
	let mut bs = Bitspace::new();
	println!("bit at 0: {:?}", bs.at(0,0,0));
	println!("bit at 0, material_id: {:?}", bs.at(0,0,0).material_id());
	
	for i in 0 .. bs.sx {
		for j in 0 .. bs.sy {
			for k in 0 .. bs.sz {
				if rand::random() {
					//let bx = &b2;
                    //let bx = Bit::new();
                    let bx = make_oil_bit();
					bs.set(i, j, k, bx);
                    //bs.list();
				}
			}
		}
	}
	
	for x in 0 .. bs.sx {
		for y in 0 .. bs.sy {
			for z in 0 .. bs.sz {
				let mut b = bs.at_mut(x, y, z);
				match b.btype {
					BitType::Empty => println!("its empty"),
					BitType::Oil =>
                    {
                        println!("its an oil bit");
                        //let mut f = b.flameable.take().unwrap();
                        let mut f = b.flameable.as_mut().unwrap();
                        f.ignite();
                        /*
                        f.update(1.0);
                        for i in 0 .. 10 {
                            f.update(1.0);
                        }
                        */
                    },
					_ => (),
				}
			}
		}
	}

    bs.list();

    println!("BurnSystem test:");
    let mut burn_sys = BurnSystem::new(bs);
    for i in 0..3 {
        burn_sys.update(0.1);
    }

	println!("done");
}
