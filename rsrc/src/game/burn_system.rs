use game::*;

pub struct BurnSystem {
    bs: Bitspace,
}

impl BurnSystem {
    pub fn new(bs: Bitspace) -> Self {
        BurnSystem {
            bs: bs,
        }
    }
    
    pub fn update(&mut self, dt: f32) {

        for x in 0..self.bs.sx {
            for y in 0..self.bs.sy {
                for z in 0..self.bs.sz {
                    let mut b = self.bs.at_mut(x, y, z);
                    println!("{:?}", b);
                    match b.flameable {
                        Some(ref mut f) =>
                        {
                            if f.active {
                                f.update(dt)
                            }
                        },
                        None => (),
                    }
                }
            }
        }
    }
}
