use std::rc::Rc;
use std::cell::RefCell;
use game::*;

pub fn make_empty_bit() -> Bit {
    Bit::new()
}

pub fn make_oil_bit() -> Bit {
    let mut b = make_empty_bit();
    b.btype = BitType::Oil;

    let mut boxed_bit = Rc::new(RefCell::new(b));

    {
    //let f = Some(Flameable::new(Rc::new(RefCell::new(&b))));
    let f = Some(boxed_bit.clone());
    //let f = Some(Flameable::new(&b));
   // b.flameable = f;
    }
    let mut r = boxed_bit.borrow_mut();
    r
}

pub fn make_wood_bit() -> Bit {
    let mut b = make_empty_bit();
    b.btype = BitType::Wood;

    b.destroyable = Some(Destroyable::new());
    b
}
