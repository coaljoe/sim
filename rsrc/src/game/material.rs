use game::*;

static mut next_id: i32 = -1;

#[derive(Debug,Copy,Clone)]
pub enum MaterialType { NotSet, Sand, Wood, Iron, Oil }

#[derive(Debug,Copy,Clone)]
pub struct Material {
    pub id: i32,
	typ: MaterialType,
	density: i32,
	corrosionResistance: f32,
	pub hardness: f32,
	// flameable properties
    //flameTime: f32
    pub igniteTemp: f32,
    pub tempInc: f32,
    pub wetness: f32,
}

impl Material {
	pub fn new() -> Material {
        let mut id = 0;
        unsafe { next_id += 1; id = next_id}
		Material {
            id: id,
			typ: MaterialType::Sand,
			density: 1000,
			corrosionResistance: 0.0,
			hardness: 0.0,
            igniteTemp: 100.0,
			tempInc: 1.0,
            wetness: 0.0,
		}
	}
  
	pub fn set_density(&mut self, v: i32) {
		self.density = v
	}
  
	pub fn density(self) -> i32 {
		self.density
	}
}

// materials
//static sandMaterial: Material = Material::new();
//static defaultMaterial: Material = &sandMaterial as *const Material;
//const sandMaterial: Material = Material::new();
//const defaultMaterial: Material = sandMaterial;

