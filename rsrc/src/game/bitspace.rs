#[macro_use]
use ndarray::prelude::*;
use game::*;

const sx: usize = 2;//32;
const sy: usize = 2;//32;
const sz: usize = 1;//32;

pub struct Bitspace {
	//bits: Vec<Vec<Bit>>;
	//bits: [[[Bit; sx]; sy]; sz],
    bits: Array3<Bit>,
	pub sx: i32,
	pub sy: i32,
	pub sz: i32,
}

impl Bitspace {
	pub fn new() -> Bitspace {
		/*
		let bits = Vec::new();

		for i in 0 .. sx {
			bits.push(Vec::new())
			bits[i as usize] = Bit::EmptyBit
			for j in 0 .. sy {
				bits[i as ]
				for k in 0 .. sz {
					
				}
			}
		}
		*/
		
		/*
		let bits: [[[Bit; sx]; sy]; sz];
		
		for i in 0 .. sx {
			for j in 0 .. sy {
				for k in 0 .. sz {
					bits[i][j][k] = Bit::EmptyBit
				}
			}
		}
		*/
		
		println!("creating bitspace...");

        let b = Bit::new();
        //let _bs = vec![b; sx*sy*sy];

		Bitspace {
			//bits: [[[Bit::EmptyBit; sx]; sy]; sz],
            //bits: Array3::<Bit>::from_elem(b, 0),
            bits: Array3::<Bit>::from_elem((sx, sy, sz), b),
            //bits: Array3::<Bit>::zeros((sx, sy, sz)),
			sx: sx as i32,
			sy: sy as i32,
			sz: sz as i32,
		}
	}
	
	pub fn set(&mut self, x: i32, y: i32, z: i32, b: Bit) {
		//self.bits[x as usize][y as usize][z as usize] = b;
		self.bits[[x as usize, y as usize, z as usize]] = b;
	}

	pub fn at(&self, x: i32, y: i32, z: i32) -> &Bit {
		//self.bits[[x as usize, y as usize, z as usize]].clone()
		self.bits.get((x as usize, y as usize, z as usize)).unwrap()
	}
	
	pub fn at_mut(&mut self, x: i32, y: i32, z: i32) -> &mut Bit {
		//self.bits[[x as usize, y as usize, z as usize]].clone()
		self.bits.get_mut((x as usize, y as usize, z as usize)).unwrap()
	}

    pub fn list(&self) {
        println!("\n-- BitSpace.list begin");
        for x in 0..self.sx {
            for y in 0..self.sy {
                for z in 0..self.sz {
                    let b = self.at(x, y, z);
                    println!("{} {} {} -> {:?}", x, y, z, b);
                }
            }
        }
        println!("-- BitSpace.list end\n");
    }
}
