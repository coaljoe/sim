use game::*;

#[derive(Debug,Clone)]
pub struct MaterialSys {
	materials: Vec<Material>,
}

impl MaterialSys {
	pub fn new() -> MaterialSys {
		MaterialSys {
            materials: Vec::new(),
		}
	}
  
	pub fn add(&mut self, m: Material) {
		self.materials.push(m);
	}
  
	pub fn remove(&mut self, m: Material) {
		self.materials.remove(m.id as usize);
	}

    pub fn get_material_by_id(&self, id: i32) -> Material {
        *self.materials.get(id as usize).unwrap()
        /*
        let m: Material;
        let mut found = false;

        for x in &self.materials {
            if x.id == id {
                m = *x;
                found = true;
            }
        }
        return m;
        */
        /*
        if !found {
            panic!("material not found");
        }
        */
        //m
        //self.materials[id]
    }

    //pub fn get_material_by_id_mut(&self, id: i32) -> &mut Material {
    //}

    pub fn derp(&self) {
        println!("material sys: do nothing");
    }
}

impl Default for MaterialSys {
    fn default() -> Self {
        MaterialSys::new()
    }
}
