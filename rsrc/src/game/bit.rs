use std::rc::Rc;
use std::cell::{Cell,RefCell};
//use std::num::Zero;
use game::*;

static mut max_id: i32 = -1;

//pub trait Bit {
//    fn data() -> BitData;
//}
pub type Bit = BitData;

#[derive(Debug,Clone)]
pub struct BitData {
	pub id: i32,
    pub btype: BitType,
    material_id: i32,
	//pub material: Material,
	pub flameable: Option<Flameable>,
    pub destroyable: Option<Destroyable>,
	pub temp: f32,
    pub dirty: bool,
}

#[derive(Debug,Clone)]
pub struct Flameable {
	//pub bit: Cell<BitData>,
    //pub bit: Box<BitData>,
    pub bit: Rc<RefCell<BitData>>,
    //pub bit: Box<BitData>,
	pub flameTime: f32,
	pub curFlameTime: f32,
	pub active: bool,
}

#[derive(Debug,Clone)]
pub struct Destroyable {
	fatique: i32
}

#[derive(Debug,Clone)]
pub enum BitType { Empty, Oil, Wood }

/*
#[derive(Debug,Copy,Clone)]
pub enum Bit {
	EmptyBit,
	OilBit (BitData, Flameable),
	WoodBit (BitData, Flameable, Destroyable),
}
*/

/*
#[derive(Debug,Clone)]
struct OilBit {
    pub data: BitData,
    //pub flameable: Flameable,
}

impl OilBit {
    pub fn new() -> OilBit {
        OilBit {
            data: OilBit {

            }
        }
    }
}
*/

impl BitData {
    pub fn new() -> BitData {
        let mut id = 0;
        unsafe { max_id += 1; id = max_id}
        BitData {
            id: id,
            btype: BitType::Empty,
            material_id: 0,
            flameable: None,
            destroyable: None,
            temp: 0.,
            dirty: false,
        }
    }

    pub fn material_id(&self) -> i32 {
        self.material_id
    }

    pub fn set_material(&mut self, m: Material) {
        self.material_id = m.id;
    }

    pub fn get_material(&mut self) -> Material {
        material_sys().get_material_by_id(self.material_id)
    }

}

/*
impl Zero for BitData {
    fn zero() -> BitData {
        BitData::new()
    }
}
*/

impl Flameable {
	pub fn new(b: Rc<RefCell<BitData>>) -> Flameable {
	//pub fn new(b: &BitData) -> Flameable {
		Flameable {
			//bit: Cell::new(b),
            //bit: Rc::new(b),
            //bit: Box::new(*b),
            bit: b,
			flameTime: 10., // prop
			curFlameTime: 0.,
			active: false,
		}
	}

	pub fn ignite(&mut self) {
		println!("igniting flameable...");
		self.active = true;
	}
	
	pub fn update(&mut self, dt: f32) {
		if !self.active {
			return
		}
		
		self.curFlameTime += dt;
		
		if self.curFlameTime < self.flameTime {
			println!("Flameable: still flaming...");
			//let mut b = self.bit.get();
			//self.bit.temp += self.bit.material.tempInc * dt
		} else {
			println!("Flameable: burned down");
			
			// reset
			self.curFlameTime = 0.;
			self.active = false;
		}
	}
}

impl Destroyable {
    pub fn new() -> Self {
        Destroyable {
           fatique: 0, 
        }
    }
}
