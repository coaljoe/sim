import lib/ry/ry
import lib/ry/node
import lib/ry/ecs

proc main() =
  echo "main()"
  let ry = newRy()
  ry.init()

  var n: Entity[float]
  var q = newEmptyNode()
  echo q

  while ry.step():
    let dt = ry.dt
    echo "update ", dt

  echo "done"


if isMainModule:
  main()
