import tables

type
  A = ref object of RootObj
    v : int

  B = ref object of A
    v2 : int

  C = ref object of A
    v2 : int

var a = B(v: 1, v2: 11)
var b = C(v: 2, v2: 22)

var s : seq[A]
#var s = @[a, b]
s = newSeq[A]()
s.add(a)
s.add(b)
var q = initTable[int, A]()
q[0] = a
q[1] = b
echo q[0].v
echo cast[B](q[0]).v2 # downcast
echo B(q[0]).v2 # explicit downcast
echo q[0].B.v2 # type convertion
echo ((B)q[0]).v2 # c-style convertion
echo q[0].B[]
echo repr(q[0].B)
#echo q[0].v2

for x in s:
  echo x.v
  #echo x.v2
