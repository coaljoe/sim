import glm
import material

type
  Bitspace* = ref object
    dim* : Vec3[int]
    xbits* : seq[seq[seq[Bit]]] # not nil?
  
  # entity
  Bit* = ref object
    material* : Material not nil
    bitspace* : Bitspace
    flameable* : Flameable
    destroyable* : Destroyable
    ## generic properties
    age* : int
    # position in bitspace
    pos* : Vec3[int]
    # потеряно от прочности в %
    fatique* : float
    temp*: float
    # properties
    #props* : Props
    
  Flameable* = ref object
    bit* : Bit
    curFlameTime* : float

  Destroyable* = ref object
    bit* : Bit
    # потеряно от прочности в %
    derpFatique* : float
    
  Bitspace2* = ref object
    dim* : Vec3[int]
    xbits* : seq[seq[seq[OnlyTemp]]]

  #OnlyTemp* = object
  OnlyTemp* = ref object
    temp* : float
    #temp* : int8
    
