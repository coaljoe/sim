import item

proc main() =
  let it = newItem()
  echo it.name
  it.name = "test"
  echo it.name
  echo it.isWeapon()
  echo it.isAppliable()
  echo it.isWeapon

  let it2 = newAppliableItem()
  #echo it2
  echo it.isAppliable, " ", it2.isAppliable
  echo it.invIcon, " ", it2.invIcon
  it2.setApplyMessage("test apply message!")
  apply(it2)

if isMainModule:
  main()
