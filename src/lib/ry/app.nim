import times
import glm
import glfw
import glfw/wrapper
import timer

## Window

type
  Window = ref object
    glfwWin : glfw.Win


proc newWindow*(): Window =
  let w = Window()
  return w


proc init(w:Window) =
  glfw.init()

  let win = newGlWin(
    dim = (w: 960, h: 540),
    title = "ry",
    resizable = false,
    version = glv21
  )
  w.glfwWin = win

  glfw.makeContextCurrent(w.glfwWin)

  #if not gladLoadGL(getProcAddress):
  #  quit "Error initialising OpenGL"

  glfw.swapInterval(1)

  #init()


proc hasExit*(w:Window): bool =
  w.glfwWin.shouldClose


proc update(w:Window, dt:float) =
  glfw.swapBufs(w.glfwWin)
  glfw.pollEvents()


## App

type
  App* = ref object
    win* : Window
    timeLast : float
    dt* : float


proc newApp*(): App =
  let app = App(
    win: newWindow(),
    timeLast: 0.0
  )
  return app


proc init*(a:App) =
  a.win.init()


proc quit*(a:App) =
  echo "app.quit"


proc update(a:App, dt:float) =
  a.win.update(dt)


proc step*(a:App): bool =
  if a.win.hasExit():
    a.quit()
    return false

  let timeNow = epochTime()

  # first frame
  if a.timeLast == 0:
    a.timeLast = timeNow
  
  a.dt = timeNow - a.timeLast
  #echo timeNow, " ", a.timeLast, " ", a.dt
  a.timeLast = timeNow

  a.update(a.dt)
  return true

