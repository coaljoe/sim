#type
#  Component*[T] = ref object of RootObj

type
  Entity*[T] = ref object of RootObj
    id : int
    components : seq[T]

type
  System* = ref object

var
  maxEntityId = -1

proc newEntity* [T]: Entity[T] =
  inc maxEntityId
  return Entity(id: maxEntityId,
                components: newSeq[T]())


proc addComponent* [T](e:Entity, c:T) =
  e.components.add(c)


proc getComponent* [T](e:Entity, c:T): T =
  for x in e.components:
    if x == c:
      return x
  raise "derp"
