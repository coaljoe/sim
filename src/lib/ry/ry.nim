import app, renderer

type
  Ry* = ref object
    #win : Window
    app* : App
    renderer*: Renderer


proc newRy*(): Ry =
  let ry = Ry(
    app: newApp(),
    renderer: newRenderer())
  ry


proc init*(r:Ry) =
  r.app.init()


proc step*(r:Ry): bool =
  r.app.step()


proc dt*(r:Ry): float =
  return r.app.dt
