#type
#  Renderable* = concept s
#    xrender(s) is s

type
  Renderer* = ref object
    #renderables*: seq[Renderable]


proc newRenderer* : Renderer =
  result = Renderer()
    #renderables: newSeq[Renderable](),


proc render* (r:Renderer) =
  discard 
