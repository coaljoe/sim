import ecs
import transform

proc newEmptyNode* : Entity =
  let en = newEntity()
  en.addComponent(newTransform())
  return en
