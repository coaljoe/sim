import glm
import ecs

# component
type
  Transform* = ref object
    mat* : Mat4x4[float]
    pos : Vec4[float]
    rot : Vec4[float]
    scale : Vec4[float]
    dirty : bool

proc build (t:Transform)

proc newTransform* : Transform =
  let t = Transform(
    mat: mat4(),
    pos: vec4(),
    rot: vec4(),
    scale: vec4(),
    dirty: true)
  t.build()
  t

proc `pos=`* (t: var Transform, v:Vec4) =
  t.pos = v
  t.build()

proc pos* (t:Transform): Vec4 =
  return t.pos

proc `rot=`* (t: var Transform, v:Vec4) =
  t.rot = v
  t.build()

proc rot* (t:Transform): Vec4 =
  return t.rot


proc build (t:Transform) =
  echo "derp"

