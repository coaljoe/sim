var maxId = -1

type
  MaterialType* {.pure.} = enum
    none, sand, wood, iron, oil

  Material* = ref object
    id* : int
    typ* : MaterialType
    # generic properties
    density* : int
    #corrosivity*: int # water c
    # сопротивление коррозии (в %?)
    corrosionResistance*: float
    # прочность (mohs or brinell?)
    hardness* : float
    # горючесть 0-1
    flameability* : float
    # вязкость 0-1
    viscosity* : float
    # stiffness
    # strenght
    
    ## flameable properties
    flameTime* : float
    tempInc* : float


proc newMaterial* (typ:MaterialType, density:int, flameability = 0.0, viscosity = 0.0,
                   hardness = 10.0, flameTime = 0.0, tempInc = 0.0): Material not nil =
  maxId += 1
  return Material(
    id: maxId,
    typ: typ,
    density: density,
    flameability: flameability,
    viscosity: viscosity,
    hardness: hardness,
    flameTime: flameTime,
    tempInc: tempInc,
  )


proc setType* (m:Material, typ:MaterialType) =
  m.typ = typ


proc name* (m:Material): string =
  return $m.typ


# library of materials
#
let
  sandMaterial* = newMaterial(typ = MaterialType.sand, density = 1600, viscosity = 0.1, hardness = 3.0)
  woodMaterial* = newMaterial(typ = MaterialType.wood, density = 700, flameability = 0.5, hardness = 3.0)
  ironMaterial* = newMaterial(typ = MaterialType.iron, density = 7870, hardness = 4.0)
  oilMaterial* = newMaterial(typ = MaterialType.oil, density = 900, viscosity = 0.2, flameability = 0.8, hardness = 0, flameTime = 0.5, tempInc = 1)
  defaultMaterial* = sandMaterial

