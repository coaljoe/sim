#import subfield
import times
import types
import bitspace
import bitspace2
import bit
import bits
import material
import random

proc main_sim() =
  GC_disableMarkAndSweep()

  let bs = newBitspace(2, 2, 2)
  echo "main_sim"
  let b = newBit(bs)
  echo b[]
  echo repr(b)
  echo b
  b.setMaterial(woodMaterial)
  echo b.show()
  echo b.strenth
  b.setMaterial(ironMaterial)
  echo b.show()
  var b1: Bit
  deepCopy(b1, oilBit)
  echo b1.show
  b1.age = 10
  echo b1[]
  #var b2: Bit
  #deepCopy(b2, oilBit)
  var b2 = oilBit.clone
  b2.fatique = 0.5
  echo b2[]
  echo b2.strenth

  #let bs = newBitspace(4, 4, 4)
  echo bs.xbits
  echo bs.xbits[0][0][0].show

  echo b2.flameable.flaming
  b2.flameable.ignite
  echo b2.flameable.flaming
  for x in 0..10:
    b2.update(0.1)
    echo b2.temp
  echo b2.flameable.flaming

  echo repr b2.pos
  bs.clearBit(0,0,0)
  bs.setBit(b2, 0,0,0)
  echo repr b2.pos
  #echo bs.getBitsNeighbours(b2)
  #echo bs.getBitsNeighbours(b2)
  
  #let sx, sy, sz = 1000, 1000, 1
  let
    #sx = 1000
    #sy = 1000
    sy = 16*128
    sx = 32*128
    sz = 1
  echo "bits: ", sx*sy*sz
  let bs2 = newBitspace2 (sx, sy, sz)
  #bs2.showLayer
  #bs2.fill(oilBit)
  
  
  let t0 = cpuTime()
  echo "updating..."
  for x in 0 .. <sx:
    for y in 0 .. <sy:
      for z in 0 .. <sz:
        #echo x, y, z
        var b = bs2.getBit2 (x, y, z)
        b.temp = random(1.0)
        
        # update for non ref version
        #bs2.xbits[x][y][z] = b
  echo "done"
  echo "CPU time [s] ", cpuTime() - t0, " ", (cpuTime() - t0) * 1000
  echo "mem ", getTotalMem()/1024/1024, " ", getOccupiedMem()/1024/1024
  
  echo bs2.xbits[1][1][0].temp
  echo "done"

if isMainModule:
  main_sim()
