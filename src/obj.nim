var maxObjId = 0

# component
type Obj* = ref object
  id*: int

proc newObj*(): Obj =
  var o = new(Obj)
  o.id = maxObjId
  maxObjId += 1
  o
