import sequtils
import glm
import types
import bit

## Static bits space
##
#type
#  Bitspace2* = ref object
#    dim* : Vec3[int]
#    bits* : seq[seq[seq[Bit]]] # not nil?

proc init (bs:Bitspace2)

proc newBitspace2* (x,y,z:int): Bitspace2 =
  let bs = Bitspace2(dim: vec3(x, y, z))
  bs.init()
  return bs

  
iterator bits* (bs:Bitspace2) : var OnlyTemp =
  for x in 0 .. <bs.dim.x:
    for y in 0 .. <bs.dim.y:
      for z in 0 .. <bs.dim.z:
        yield bs.xbits[x][y][z]

        
proc init (bs:Bitspace2) =
  echo ":Bitspace2.init"
  #bs.bits = @[]
  #bs.bits.add @[]
  #bs.bits[0].add @[]
  #bs.bits[0][0].add @[]
  #var b = sandBit.clone
  #bs.bits =  @[@[@[b],@[b,b],@[b,b,b]]]
  let
    sx = bs.dim.x
    sy = bs.dim.y
    sz = bs.dim.z

  assert (sx != 0 and sy != 0 and sz != 0)
    
  echo bs.dim
  echo "herp"

  # fixme:
  # different sx, sy, sz sizes not checked
 
  bs.xbits = newSeq[seq[seq[OnlyTemp]]](sx)
  for x in 0 .. <sx:
    bs.xbits[x] = newSeq[seq[OnlyTemp]](sy)
    for y in 0 .. <sy:
      bs.xbits[x][y] = newSeq[OnlyTemp](sz)
      for z in 0 .. <sz:
        bs.xbits[x][y][z] = OnlyTemp(temp: 0)
  #var xbits2d = newSeqWith(sy, newSeq[Bit](sx))
  #bs.xbits = newSeq[xbits2d](sz)
  #bs.xbits = newSeqWith(sz, xbits2d)
  ## buggy with -d:release
  #bs.xbits = newSeqWith(sx, newSeqWith(sy, newSeq[Bit](sz)))
  
  #echo bs.xbits[0][0][0] == nil
  #echo bs.xbits[1][1][1] == nil
  #echo bs.xbits[2][2][2] == nil
  echo "derp ", bs.dim

  #[
  # fill bits
  for x in 0 .. <sx:
    for y in 0 .. <sy:
      for z in 0 .. <sz:
        bs.xbits[x][y][z] = sandBit.clone
  ]#
  
  for xb in bs.bits:
    #echo "a"
    #echo repr xb
    #echo xb[]
    #if xb != nil:
    #echo xb[]
    #xb = sandBit.clone
    #xb = oilBit.clone
    xb = OnlyTemp(temp: 0)

    
  echo ":done"

proc getBit2* (bs:Bitspace2, x,y,z:int): OnlyTemp =
  bs.xbits[x][y][z]