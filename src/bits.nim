#import subfield
#import bit
import types

#type
#  Flameable* = ref object
#    bit* : Bit
#    flameTime* : float
#    curFlameTime* : float

#func newFlameable (bit:Bit): Flameable =
#  Flameable(bit: Bit, flaming: false)


proc ignite* (f:Flameable) =
  f.curFlameTime = 0.01


proc flaming* (f:Flameable): bool =
  f.curFlameTime != 0


proc update* (f:Flameable, dt:float) =
  if not f.flaming:
    return

  f.curFlameTime += dt
  if f.curFlameTime >= f.bit.material.flameTime:
    f.curFlameTime = 0
    return

  f.bit.temp += f.bit.material.tempInc * dt


#type
#  Destroyable* = ref object
#    bit : Bit
#    # потеряно от прочности в %
#    derpFatique* : float

proc update* (d:Destroyable, dt:float) =
  discard

##
# wood <- destroyable, flameable
# oil <- flameable
# concrete <- destroyable
