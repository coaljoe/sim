import obj
#{.this: x.}

const
  icUnknown* = 0
  icGood* = 1
  icBad* = 2
  icBroken* = 3

type
  #AppliableItem = ref object
  #AppliableItem
  #EquipableItem = ref object
  ItemContainer = ref object of RootObj
  Weapon = ref object of RootObj
  Character = ref object of RootObj

type
  ItemCondition = int

#type
#  ItemI = concept

##  Предмет.
##  Может содержать множество ссылок на себя, передается всегда как референс.
##  Один предмет может храниться только в одном контейнере.
# entity
type
  Item* = ref object of RootObj
    weapon* : Weapon
    name* : string
    weight* : float
    bulk* : float
    condition* : ItemCondition
    durability* : float
    description* : string
    longDescription* : string
    # item's holder/host
    cont* : ItemContainer
    # inventory
    invIcon* : string

  # component (not exported)
  AppliableItem = ref object of Item
    #item: Item # ref item?
    #applyMessage: const string
    applyMessage : string

  # component
  EquipableItem* = ref object of Item
    #item*: Item #ref Item
    wearer* : Character #ref Character
    insulation* : int
    itemSlotId* : string

  ## ???
  ## AppliableEquipableItem?


proc newItem*(): Item =
  Item(
    #mObj = newObj()
    condition: icUnknown,
    durability: 1.0,
    invIcon: "noimage",
  )


proc isAppliable*(x:Item): bool =
  x of AppliableItem


proc isEquipable*(x:Item): bool =
  x of EquipableItem


proc isWeapon*(x:Item): bool =
  x.weapon != nil


proc inContainer*(x:Item): bool =
  x.cont != nil


proc setContainer*(x:Item, cont:ItemContainer) =
  x.cont = cont


proc unsetContainer*(x:Item) =
  x.cont = nil


proc destroy*(x:Item) =
  #_ItemSys.removeItem(x)
  discard 


proc `$`*(x:Item): string =
  return "<Item name: " & x.name & ">"


#### Appliable Item ####

type
  AppliableItemI = concept s
    apply(s)


proc newAppliableItem*(): AppliableItem =
  let r = AppliableItem()
  r.applyMessage = "bad"
  r

proc setApplyMessage*(x:AppliableItem, s:string) =
  x.applyMessage = s

proc apply*(ai:AppliableItem) =
  echo "item applied; msg: ", ai.applyMessage
  #echo "item name:", ai.item.name
  echo "item name:", ai.name


#### Equipable Item ####

proc newEquipableItem(x:Item): EquipableItem =
  new(result)
  #result.item = x

