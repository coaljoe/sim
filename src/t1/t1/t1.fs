module t1
open bit
open bits
//open bitspace
//open material


[<EntryPoint>]
let main argv =
    let b1 = newBitData()
    printfn "%f" b1.temp
    b1.temp <- 10.0
    printfn "%f" b1.temp
    printfn "test"
    showBit b1

    let zov = OilBit (newBitData(), {flameTime=0.0; curFlameTime=0.0})
    printfn "%A" zov
    let (OilBit (bd, f)) = zov
    printfn "derp"
    showBit bd
    printfn "derp"

    match zov with
        | OilBit (b, f) ->
            printfn "its ZOil" 
            showBit b
            ignite f
        | WoodBit (b, f, d) ->
            printfn "its ZWood"
        | _ -> 
            failwith "match fail"

    let rand = System.Random()

    //let bs = newBitspace 3 3 3
    //let sx, sy, sz = 3, 3, 3
    //let sx, sy, sz = 100, 100, 2
    //let sx, sy, sz = 1000, 1000, 2
    let sx, sy, sz = 1000, 1000, 1
    //let sx, sy, sz = 64*2048, 64*2048, 1
    //let sx, sy, sz = 32*128, 32*128, 1
    printfn "bits: %d" (sx*sy*sz)
    let bs = bitspace.newBitspace sx sy sz
    printfn "%d" bs.id

    //let b3 = createBit()
    //bitspace.setBit b3 0 0 0 bs

    let (OilBit(bd, f)) = bitspace.getBit 0 0 0 bs
    printfn "b4 temp: %f" bd.temp

    let stopWatch = System.Diagnostics.Stopwatch.StartNew()
    printfn "processing bits..."
    for x = 0 to sx-1 do
      for y = 0 to sy-1 do
        for z = 0 to sz-1 do
          let b = bitspace.getBit x y z bs // get bit
          let (OilBit(bd, f)) = b // deconstruct bit
          //printfn "%d" bd.id
          //bd.temp <- bd.temp + 1.0
          bd.temp <- bd.temp + rand.NextDouble()
    printfn "done"
    stopWatch.Stop()
    printfn "elapsed ms: %f" stopWatch.Elapsed.TotalMilliseconds
    let x = System.GC.GetTotalMemory(false)
    printfn "mem: %d MB" (int(x)/1024/1024)
    let x1 = System.GC.GetTotalMemory(true)
    printfn "mem1: %d MB" (int(x1)/1024/1024)


    (* object reference checks *)

    let (OilBit(bd, f)) = bitspace.getBit 0 0 0 bs
    printfn "b4 temp: %f" bd.temp

    printfn "b %d" bd.material.density
    bd.material.setDensity 99
    printfn "a %d" bd.material.density

    let (OilBit(bd5, f)) = bitspace.getBit 1 1 0 bs
    printfn "bd5 b: %d" bd5.material.density

    printfn "%A" argv
    0 // return an integer exit code
