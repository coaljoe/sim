module bit
open material
open bits

let mutable _maxBitId = 0

type BitData = {
  id: int;
  material: Material;
  flameable: Flameable option;
  mutable temp: float;
}

//type zBit = Q of int * int | A of int * int * int
type Bit = 
  | EmptyBit
  | OilBit of BitData * Flameable 
  | WoodBit of BitData * Flameable * Destroyable

let newBitData () =
  //printfn "newBitData"
  let bd = {id = _maxBitId;
    material = defaultMaterial;
    flameable = None;
    temp = 0.0}
  _maxBitId <- _maxBitId + 1
  //printfn "-> %d" _maxBitId
  //failwith "2"
  bd

let createBit () =
    //printfn "createBit"
    OilBit (newBitData(), {flameTime=0.0; curFlameTime=0.0})

let showBit b =
  printfn "<Bit %s>" b.material.name