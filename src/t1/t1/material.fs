module material
[<RequireQualifiedAccess>] 
type MaterialType = NotSet | Sand | Wood | Iron | Oil

type Material(
    typ: MaterialType,
    density: int,
    corrosionResistance: float,
    hardness: float) =

    let mutable _density = density

    new () = Material(MaterialType.Sand, 1000, 0.0, 0.0)

    member this.name :string = typ.ToString()
    member this.density = _density
    member this.setDensity v = _density <- v

let sandMaterial = Material()
let defaultMaterial = sandMaterial

(*
type Material =
  val typ: int
  val density: int
  val corrosionResistance: float
  val hardness: float

  new () = {
    typ = 0
    density = 1000
    corrosionResistance = 0.0
    hardness = 0.0
  }
*)
