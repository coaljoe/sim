module bitspace
open bit

let mutable _maxId = 0

//type Bitfield =
type T =
    { id : int
      xbits : Bit[,,] }

let newBitspace sx sy sz =
    let bs = {id = _maxId; xbits = Array3D.init sx sy sz (fun a b c -> createBit())}
    _maxId <- _maxId + 1
    bs

let setBit b x y z bs =
    printfn "%d" bs.id
    bs.xbits.[x, y, z] <- b

let getBit x y z bs =
    bs.xbits.[x,y,z]