#import subfield
import glm
import types
import material
import bitspace
import bits

const
  bitSize = 10
  bitVolume = bitSize*3


proc newBit* (bs:Bitspace, material = defaultMaterial): Bit =
  Bit(
    age: 0,
    material: material,
    bitspace: bs,
  )


proc mass* (b:Bit): int =
  bitSize * b.material.density


proc strenth* (b:Bit): float =
  b.material.hardness * (1.0 - b.fatique)


proc clone* (b:Bit): Bit =
  deepCopy(result, b)


proc setMaterial* (b:Bit, mat:Material not nil) =
  #b.material.setType(mtype)
  b.material = mat


proc `$`* (b:Bit): string =
  #return ""
  return "<Bit " & b.material.name & ">"


proc show* (b:Bit): string =
  return "<Bit material: " & b.material.name & ">"


proc update* (b:Bit, dt:float) =
  #let f = b.flameable
  #f.xupdate(dt)
  if b.flameable != nil:
    b.flameable.update(dt)
  if b.destroyable != nil:
    b.destroyable.update(dt)


# bit factory
#

let oilBit* = newBit(nil, oilMaterial)
# add flameable
oilBit.flameable = Flameable(bit: oilBit)

let sandBit* = newBit(nil, sandMaterial)
# add destroyable
sandBit.destroyable = Destroyable(bit: sandBit)

