import sequtils
import glm
import types
import bit

## Static bits space
##
#type
#  Bitspace* = ref object
#    dim* : Vec3[int]
#    bits* : seq[seq[seq[Bit]]] # not nil?

proc init (bs:Bitspace)

proc newBitspace* (x,y,z:int): Bitspace =
  let bs = Bitspace(dim: vec3(x, y, z))
  bs.init()
  return bs

  
iterator bits* (bs:Bitspace) : var Bit =
  for x in 0 .. <bs.dim.x:
    for y in 0 .. <bs.dim.y:
      for z in 0 .. <bs.dim.z:
        yield bs.xbits[x][y][z]

        
proc init (bs:Bitspace) =
  echo ":Bitspace.init"
  #bs.bits = @[]
  #bs.bits.add @[]
  #bs.bits[0].add @[]
  #bs.bits[0][0].add @[]
  #var b = sandBit.clone
  #bs.bits =  @[@[@[b],@[b,b],@[b,b,b]]]
  let
    sx = bs.dim.x
    sy = bs.dim.y
    sz = bs.dim.z

  assert (sx != 0 and sy != 0 and sz != 0)
    
  echo bs.dim
  echo "herp"

  # fixme:
  # different sx, sy, sz sizes not checked
 
  bs.xbits = newSeq[seq[seq[Bit]]](sx)
  for x in 0 .. <sx:
    bs.xbits[x] = newSeq[seq[Bit]](sy)
    for y in 0 .. <sy:
      bs.xbits[x][y] = newSeq[Bit](sz)
      for z in 0 .. <sz:
        bs.xbits[x][y][z] = nil # remove?
  #var xbits2d = newSeqWith(sy, newSeq[Bit](sx))
  #bs.xbits = newSeq[xbits2d](sz)
  #bs.xbits = newSeqWith(sz, xbits2d)
  ## buggy with -d:release
  #bs.xbits = newSeqWith(sx, newSeqWith(sy, newSeq[Bit](sz)))
  
  echo bs.xbits[0][0][0] == nil
  #echo bs.xbits[1][1][1] == nil
  #echo bs.xbits[2][2][2] == nil
  echo "derp ", bs.dim

  #[
  # fill bits
  for x in 0 .. <sx:
    for y in 0 .. <sy:
      for z in 0 .. <sz:
        bs.xbits[x][y][z] = sandBit.clone
  ]#
  
  for xb in bs.bits:
    #echo "a"
    #echo repr xb
    #echo xb[]
    #if xb != nil:
    #echo xb[]
    #xb = sandBit.clone
    xb = oilBit.clone
    
  echo ":done"

  
proc showLayer* (bs:Bitspace, k:int = 0) =
  echo "showLayer ", bs.dim
  for i in 0 .. <bs.dim.x:
    for j in 0 .. <bs.dim.y:
      #for z in 0 .. <bs.dim.z
      var c = " "
      if bs.xbits[i][j][k] != nil:
        c = "%"
      stdout.write c & ""
    echo ""


proc getBitsNeighbourAt* (bs:Bitspace, b:Bit, x,y,z:int): Bit =
  if (x, y, z) == (0, 0, 0):
    quit("derp")

  let np = b.pos + vec3(x, y, z)

  var r: Bit
  r = nil
  try:
    r = bs.xbits[np.x][np.y][np.z]
  except IndexError:
    discard
  return r


proc getBitsNeighbours* (bs:Bitspace, b:Bit): seq[Bit] =
  let xynpos = [
    # z: -1
    (-1,-1),
    (-1, 0),
    (-1, 1),
    ( 0,-1),
    (-1, 0),
    (-1, 1),
    ( 1,-1),
    (-1, 0),
    (-1, 1),

    ( 1,-1),
    (-1, 0),
    (-1, 1),
    ( 1,-1),
    ( 0, 0),
    (-1, 1),
    ( 1,-1),
    ( 1, 0),
    (-1, 1),

    ( 1,-1),
    ( 1, 0),
    (-1, 1),
    ( 1,-1),
    ( 1, 0),
    ( 0, 1),
    ( 1,-1),
    ( 1, 0),
    ( 1, 1)]
  var npos = newSeq[(int, int, int)]()
  for z in -1..1:
    for x in xynpos:
      let v = (x[0], x[1], z)
      if v != (0, 0, 0):
        npos.add(v)

  var nbits = newSeq[Bit](npos.len)
  for x in npos:
    #let xb = bs.bits[x[0]][x[1]][x[2]]
    let xb = bs.getBitsNeighbourAt(b, x[0], x[1], x[2])
    if xb != nil:
      nbits.add(xb)

  return nbits
  #for b in bs.bitsIt:
  #  derp


proc getBit* (bs:Bitspace, x,y,z:int): Bit =
  bs.xbits[x][y][z]

proc clearBit* (bs:Bitspace, x,y,z:int) =
  if bs.xbits[x][y][z] == nil:
    echo "warn: the bit is not set, do nothing"
    return
  bs.xbits[x][y][z] = nil

proc setBit* (bs:Bitspace, b:Bit, x,y,z:int) =
  if bs.xbits[x][y][z] != nil:
    quit("derp: bit is already set")
  bs.xbits[x][y][z] = b

  # update bit's pos
  b.pos = vec3(x, y, z)


proc fill* (bs:Bitspace, b:Bit) =
  for p in bs.bits:
    var xb = b.clone()
    xb.bitspace = bs
    p = xb


proc appendBit (bs:Bitspace, b:Bit) =
  var added = false
  for x in 0..bs.dim.x-1:
    for y in 0..bs.dim.y-1:
      for z in 0..bs.dim.z-1:
        if bs.xbits[x][y][z] == nil:
          bs.setBit(b, x, y, z)
          added = true
  if not added:
    quit("derp")

